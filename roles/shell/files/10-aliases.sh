# 'ls' management
alias ls='ls --color=auto'
alias ll='ls -lAFh'
alias la='ls -a'
alias lla='ll -a'

# color for grep
alias grep='grep --color=auto'

# Some useful aliases
alias c='clear'
alias cls='clear'
alias less='less --quiet'
alias s='cd ..'
alias df='df --human-readable'
alias du='du --human-readable'
alias md='mkdir'
alias rd='rmdir'
