# Prompt color (different for root and non root users)

HOST_COLOR="\[\e[38;5;69m\]"

if [ "`id -u`" -eq 0 ]; then
  export PS1="\[\e[1;36m\]\A \[\e[1;31m\]\u\[\e[1;33m\]@$HOST_COLOR\h \[\e[1;32m\]\w \[\e[1;33m\]#\[\e[0m\] "
else
  export PS1="\[\e[1;36m\]\A \[\e[0;1m\]\u\[\e[1;33m\]@$HOST_COLOR\h \[\e[1;32m\]\w \[\e[1;33m\]$\[\e[0m\] "
fi
