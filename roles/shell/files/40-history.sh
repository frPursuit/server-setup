# Commands that start with a whitespace should not appear in the command history

export HISTIGNORE=' *'
