# Server setup

This playbook is used to setup my servers.

Usage:

    ansible-playbook -i inventory.ini main.yml [-t <tag>]

Tags:

- `web`: When an `nginx` webserver and `certbot` need to be installed
